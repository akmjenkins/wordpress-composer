module.exports = function(grunt) {

	//load tasks
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-composer');
	grunt.loadNpmTasks('grunt-shell');

	var SYNC_FILE = 'wp_import.sql';
	
	var config = {
	
		userDefaults: grunt.file.readJSON("user.defaults.json"),
	
		/**
         	 * We read in our `package.json` file so we can access the package name and version. It's already there, so
         	 * we don't repeat ourselves here.
         	 */
		pkg: grunt.file.readJSON("package.json"),
	
		/**
		 * Project Definitions
   		 */
		project: {
		},
		
		/////////////
		// composer
		/////////////
		composer: {
			options: {}
		},
		
		/////////////
		// shell
		/////////////
		shell: {
				
			checkout: {
				command:[
					'git clone https://<%= userDefaults.wordpress.repo.username %>:<%= userDefaults.wordpress.repo.password%>@<%= userDefaults.wordpress.repo.url %> ./wp-content/themes/<%= userDefaults.wordpress.theme_name %>'
				].join('&&')
			},
			
			buildassets_dev: {
				command: [
					'cd ./wp-content/themes/<%= userDefaults.wordpress.theme_name %>/',
					'sh mac_setup.sh'
				].join('&&')
			},
			
			buildassets_prod: {
				command: [
					'cd ./wp-content/themes/<%= userDefaults.wordpress.theme_name %>/assets',
					'sh mac_setup.sh'
				].join('&&')
			},
			
			wp_sync_plugins: {
				command:[
					'node ftp.js \
						--user=<%= userDefaults.wordpress.staging.ftp.user %> \
						--pass=<%= userDefaults.wordpress.staging.ftp.pass %> \
						--host=<%= userDefaults.wordpress.staging.ftp.host %> \
						--port=<%= userDefaults.wordpress.staging.ftp.port %> \
						--path=<%= userDefaults.wordpress.staging.ftp.path %> \
					'
				].join('&&')
			},
		
			wp_retrieve_database: {
				command:[
					'mysqldump \
					--host=<%= userDefaults.wordpress.staging.db.host %> \
					--user=<%= userDefaults.wordpress.staging.db.user %> \
					--password=<%= userDefaults.wordpress.staging.db.pass %> <%= userDefaults.wordpress.staging.db.name %> > ./wp/'+SYNC_FILE
				].join('&&')
			},
			
			wp_import_database: {
				command:[
					'cd ./vendor/bin',
					'sh wp db import \
						../../wp/'+SYNC_FILE+' --path=../../wp'
				].join('&&')
			},
		
			wp_generate_config: {
				command:[
					'cd ./vendor/bin',
					'sh wp core config \
						--dbname=<%= userDefaults.wordpress.local.db.name %> \
						--dbhost=<%= userDefaults.wordpress.local.db.host %> \
						--dbuser=<%= userDefaults.wordpress.local.db.user %> \
						--dbpass=<%= userDefaults.wordpress.local.db.pass %> \
						--dbprefix=<%= userDefaults.wordpress.local.db.prefix %> \
						--path=../../wp'			
				].join('&&')
			},
			
			wp_local_admin: {
				command:[
					'cd ./vendor/bin',
					'sh wp user create \
						<%= userDefaults.wordpress.local.admin_user %> \
						<%= userDefaults.wordpress.local.admin_email %> \
						--user_pass=<%= userDefaults.wordpress.local.admin_password %> \
						--role=administrator \
						--path=../../wp'			
				].join('&&')				
			},
			
			wp_install: {
				command:[
					'cd ./vendor/bin',
					'sh wp core install \
						--url=<%= userDefaults.wordpress.local.url %> \
						--title="<%= userDefaults.wordpress.local.title %>" \
						--admin_user=<%= userDefaults.wordpress.local.admin_user %> \
						--admin_password=<%= userDefaults.wordpress.local.admin_password %> \
						--admin_email=<%= userDefaults.wordpress.local.admin_email %> \
						--path=../../wp'			
				].join('&&')
			},
			
			wp_update_local_siteurl: {
				command: [
					'cd ./vendor/bin',
					'sh wp option update siteurl <%= userDefaults.wordpress.local.url %>/wp \
					--path=../../wp'			
				].join('&&')
			},
			
			wp_update_local_home: {
				command: [
					'cd ./vendor/bin',
					'sh wp option update home <%= userDefaults.wordpress.local.url %> \
					--path=../../wp'			
				].join('&&')
			}
			
		},

		/////////////
		// copy
		/////////////		
		copy: {
		
			wp_localify_database: {
				options: {
					process:function(content,srcpath) {
						return content.replace(
							new RegExp(config.userDefaults.wordpress.staging.url,'g'),
							config.userDefaults.wordpress.local.url
						);
					}
				},
				files: [
					{
						expand:true,
						cwd: './wp',
						src: [SYNC_FILE],
						dest: './wp/'
					}
				]			
			},
		
			wp_config_restore: {
				files: [
					{
						expand:true,
						cwd: './',
						src: ['wp-config.php'],
						dest: './wp/'
					}
				]
			},
		
			wp_postinstall: {
				options: {
						process:function(content,srcpath) {
							switch(srcpath) {
								case 'wp/index.php':
								
									var pieces = content.split("\n");
									for(var i = 0;i<=pieces.length;i++) {
										if(pieces[i] === "require( dirname( __FILE__ ) . '/wp-blog-header.php' );") {
											pieces[i] = "require( dirname( __FILE__ ) . '/wp/wp-blog-header.php' );"
										}	
									}
									
									return pieces.join("\n");
									
									break;
								case 'wp/wp-config.php':
									var pieces = content.split("\n");
									for(var i = 0;i<pieces.length;i++) {
										var match = pieces[i].match(/happy blogging/i);
										if(match && match.length) {
											pieces[i] += "\n define('DEV_ENVIRONMENT',TRUE);"
										//	pieces[i] += "\n define('DEV_REMOTE_BASE_URL','"+config.userDefaults.wordpress.staging.url+"');"
											pieces[i] += "\n define( 'WP_CONTENT_DIR', dirname(__FILE__) . '/wp-content' );";
											pieces[i] += "\n define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['SCRIPT_NAME']) . '/wp-content');";
										}	
									}
									return pieces.join("\n");
								
									break;
							}
							
							return content;
							
						}
				},
				files: [
					{
						cwd: './wp/wp-content',
						expand: true,
						src: ['**'],
						dest: './wp-content'
					},
					{
						expand:true,
						cwd: './wp/',
						src: ['index.php','wp-config.php'],
						dest: './'
					},
					{
						cwd: './wp/',
						src: 'index.php.sample',
						dest: './wp/index.php'
					}
				]
			}
		},
		
		/////////////
		// clean
		/////////////		
		clean: {
		
			wp_postinstall: [
					'wp/wp-content',
					'wp/index.php',
					'wp/wp-config.php' //We cannot use wp-cli after this has been removed from the ./wp directory
			],
			
			wp_syncfile: [
				'wp/'+SYNC_FILE
			]
		}

	};
	
	//Configuration
	grunt.initConfig(config);
	
	
	//////////////////////////////////////////////////////////////////////
	//	TASKS
	//////////////////////////////////////////////////////////////////////
	
	// default
	grunt.registerTask("default",[
		'install'
	]);
	
	// install
	grunt.registerTask("install",[
		'composer:install',
		'shell:wp_generate_config',
		'shell:wp_install',
		'shell:wp_update_local_siteurl',
		'postinstall',
		//'sync',
		'shell:checkout',
		'shell:buildassets_dev'
	]);
	
	// postinstall
	grunt.registerTask("postinstall",[
		'copy:wp_postinstall',
		'clean:wp_postinstall',
		'composer:update'
	]);
	
	// sync with live database
	grunt.registerTask("sync",[
		'copy:wp_config_restore', //required before we can access wp-cli
		'shell:wp_retrieve_database',
		'copy:wp_localify_database',
		'shell:wp_import_database',
		'shell:wp_local_admin', //recreate the local admin user
		'shell:wp_update_local_siteurl',
		'clean:wp_postinstall',
		'clean:wp_syncfile',
		'shell:wp_sync_plugins'
	]);

};
