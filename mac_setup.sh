#!/bin/sh


#install homebrew
echo "Installing Homebrew...";
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

#install composer globally
echo "Installing Composer...";
#curl -sS https://raw.github.com/composer/getcomposer.org/c6fedc418e1f80347724cf1bc3e86b607ac3ba30/web/installer | php -- --install-dir=/usr/local
#mv /usr/local/composer.phar /usr/local/bin/composer
php -r "readfile('https://raw.github.com/composer/getcomposer.org/c6fedc418e1f80347724cf1bc3e86b607ac3ba30/web/installer');" | php -- --install-dir=/usr/local

#install node
echo "Installing NodeJS...";
brew install node -v

#install grunt-cli globally
echo "Installing Grunt-CLI...";
npm install grunt-cli -g

#install wordpress via composer, plugins, and all other stuff
npm install
grunt install
grunt buildassets_dev
